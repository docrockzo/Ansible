    - name: Send email to user # cc: "user2@mail.com" if needed]
      community.general.mail:
        host: mail-relay.host.com
        port: 25
        to: "user1@mail.com"
        from: application@mail.com
        subject: Patch_Results
        body: |
          Results:

          {% for host in ansible_play_hosts %}
          {{ hostvars[host].results.stdout}}
          {% endfor %}
      when: results.stdout
      delegate_to: localhost
      run_once: true

#test push

